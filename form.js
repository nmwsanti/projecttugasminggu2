// Cek Validasi Form Input Data
function validateForm() {
    if (document.forms["FormInputData"]["nama"].value == "") {
        alert("Nama Tidak Boleh Kosong");
        document.forms["FormInputData"]["nama"].focus();
        return false;
    }
    if (document.forms["FormInputData"]["tgl_lahir"].value == "") {
        alert("Email Tidak Boleh Kosong");
        document.forms["FormInputData"]["tgl_lahir"].focus();
        return false;
    }
    if (document.forms["FormInputData"]["gender"].selectedIndex < 1) {
        alert("Pilih Jenis Kelamin");
        document.forms["FormInputData"]["gender"].focus();
        return false;
    }
    if (document.forms["FormInputData"]["alamat"].value == "") {
        alert("Nama Tidak Boleh Kosong");
        document.forms["FormInputData"]["alamat"].focus();
        return false;
    }
    if (document.forms["FormInputData"]["email"].value == "") {
        alert("Email Tidak Boleh Kosong");
        document.forms["FormInputData"]["email"].focus();
        return false;
    }
}

// Menampilkan Data
function tampilData(){
    var nama = document.getElementById("nama").value;
    var tgl_lahir = document.getElementById("tgl_lahir").value;
    var gender = document.getElementById("gender").value;
    var alamat = document.getElementById("alamat").value;
    var email = document.getElementById("email").value;

    alert("Nama :" + " " + nama + "\nTanggal Lahir :" + " " + tgl_lahir + "\nJenis Kelamin :" + " " + gender + "\nAlamat Rumah :" + " " + alamat + "\nEmail :" + " " + email);
}